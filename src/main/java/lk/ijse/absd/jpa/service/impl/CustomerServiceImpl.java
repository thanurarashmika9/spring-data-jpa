package lk.ijse.absd.jpa.service.impl;

import lk.ijse.absd.jpa.dto.CustomerDTO;
import lk.ijse.absd.jpa.entity.Customer;
import lk.ijse.absd.jpa.repository.CustomerRepository;
import lk.ijse.absd.jpa.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public List<CustomerDTO> getAllCustomers() {
        List<Customer> customers = customerRepository.findAll();
        return customers.stream().
                map(c->new CustomerDTO(c.getCid(),c.getName(), c.getAddress(), c.getTel()))
                .collect(Collectors.toList());
    }

    @Override
    public CustomerDTO getCustomer(String id) {
        Customer customer = customerRepository.findById(id).get();
        return new CustomerDTO(customer.getCid(), customer.getName(), customer.getAddress(),customer.getTel());
    }

    @Override
    public void saveCustomer(CustomerDTO dto) {
        customerRepository.save(new Customer(dto.getCid(), dto.getName(), dto.getAddress(),dto.getTel()));
    }

    @Override
    public void updateCustomer(CustomerDTO dto) {
        customerRepository.save(new Customer(dto.getCid(), dto.getName(), dto.getAddress(),dto.getTel()));
    }

    @Override
    public void deleteCustomer(String id) {
        customerRepository.deleteById(id);
    }
}
