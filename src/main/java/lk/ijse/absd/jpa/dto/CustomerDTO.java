package lk.ijse.absd.jpa.dto;

public class CustomerDTO {

    private String cid;
    private String name;
    private String address;
    private String tel;

    public CustomerDTO() {
    }

    public CustomerDTO(String id, String name, String address, String tel) {
        this.cid = cid;
        this.name = name;
        this.address = address;
        this.tel = tel;
    }

    public String getId() {
        return cid;
    }

    public void setId(String cid) {
        this.cid = cid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    @Override
    public String toString() {
        return "CustomerDTO{" +
                "cid='" + cid + '\'' +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", tel='" + tel + '\'' +
                '}';
    }
}