package lk.ijse.absd.jpa.repository;

import lk.ijse.absd.jpa.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer,String> {
}
