package lk.ijse.absd.jpa.main;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@ComponentScan("lk.ijse.absd.jpa")
@EnableWebMvc
@Configuration
public class WebAppConfig {
}
