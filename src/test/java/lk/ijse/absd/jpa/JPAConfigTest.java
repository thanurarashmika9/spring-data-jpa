package lk.ijse.absd.jpa;

import lk.ijse.absd.jpa.entity.Customer;
import lk.ijse.absd.jpa.main.JPAConfig;
import lk.ijse.absd.jpa.repository.CustomerRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@WebAppConfiguration
@ContextConfiguration(classes = {JPAConfig.class})
@RunWith(SpringRunner.class)
public class JPAConfigTest {

    @Autowired
    private CustomerRepository customerRepository;

    @Test
    public void testEntityManager(){
        List<Customer> all = customerRepository.findAll();
        all.forEach(System.out::println);
    }

}
